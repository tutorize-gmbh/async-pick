# AsyncPick

AsyncPick is a asynchronous replacement for `bootstrap-select`. It tries to offer the best of both worlds - you can
immediately scroll without searching and you can search your remote api.

1. [Usage](#markdown-header-usage)
    * [Initialization with rails](#markdown-header-initialization-with-rails)
    * [Backend Endpoint](#markdown-header-backend-endpoint)
2. [API](#markdown-header-api)
3. [Development](#markdown-header-development)
4. [Production](#markdown-header-building-for-production)
5. [TODO](#markdown-header-todo)

## Usage

HTML

```html
<select name="picker" id="picker-single">
  <option value>Please select</option>
  <option value="1" selected>Abbey Storrs</option>
</select>
```

JS

```javascript
// these are the default options. you can omit everything except 'id' and 'url'
new AsyncPick({
  id: 'picker-single',
  url: 'http://192.168.178.192:9000/users',
  language: document.documentElement.lang, // currently: 'de' or 'en'
  searchParam: 'q',
  pageParam: '_page',
  perPageParam: '_limit',
  jsonKey: null,
  httpMethod: 'GET',
  maxPages: 2,
  perPage: 50,
  paginateUpThreshold: 20,
  paginateDownThreshold: 80,
  searchTimeout: 400,
  searchPlaceholder: 'Search',
  searchInputClasses: [],
  withSearch: true,
  buttonClasses: [],
  emptySelectButtonText: 'Show all',
  buttonIconClasses: ['fas', 'fa-fw', 'fa-caret-down'],
  checkedIconClasses: ['fas', 'fa-fw', 'fa-check'],
  listClasses: [],
  selectedTextFormat: null, // 'count > 5'
  selectedTextVariable: '%num%', // '%x%'
  selectedText: '%num% selected', // '%x% Einträge ausgewählt'
  noResultsText: 'No results',
  dividerText: 'Selected entries:',
  resetOnClose: true,
  container: null,
  dropdownAlignRight: false,
  popupWidth: '300px',
  values: null
})
```

### Initialization with rails

#### Without preselected entry

You have to change the way you initialize `options_for_select`. Because AsyncPick loads the data via ajax anyway you do
not have to add any options to your select beforehand. Example:

Before AsyncPick: (note that all users are added to `options_for_select`)

```erb
<%= f.select :user_id,
             options_for_select(User.pluck(:full_name_reverse, :id)),
             {include_blank: t('views.defaults.form.select.choose_user')}, 
             {class: 'selectpicker form-control', data: {live_search: true, selected_text_format: 'count>3'}} %>
```

With AsyncPick you can completely omit `options_for_select` and replace it with an empty string, because all it does is
build an empty string anyway.

```erb
<%= f.select :user_id, '', {include_blank: t('views.defaults.form.select.choose_user')}, {class: 'form-control'} %>
```

#### With preselected entries

If you have preselected entries in your select then you need to add these to the select. Example:

Before AsyncPick:

```erb
<%= f.select :user_id,
             options_for_select(User.pluck(:full_name_reverse, :id), task.user_id),
             {include_blank: t('views.defaults.form.select.choose_user')}, 
             {class: 'selectpicker form-control', data: {live_search: true, selected_text_format: 'count>3'}} %>
```

With AsyncPick: (note that only the selected option is included as option)

```erb
<%= f.select :user_id, options_for_select([[task.user&.full_name_reverse, task.user_id]], task.user_id),
             {include_blank: t('views.defaults.form.select.choose_user')}, class: 'form-control' %>
```

And if you are not sure if the selected option is there (for forms that are used on new and edit for example):

```erb
<%= f.select :user_id, options_for_select([[task.user&.full_name_reverse, task.user_id].compact].reject(&:blank?), task.user_id),
             {include_blank: t('views.defaults.form.select.choose_user')}, class: 'form-control' %>
```

### Backend endpoint

#### Response structure

The above default settings expects a response like this:

```json
{
  "users": [
    {
      "text": "Peter",
      "value": "1",
      "subtext": "#some-id-1",
      "disabled": false
    },
    {
      "text": "Dirk",
      "value": "2",
      "subtext": "#some-id-2",
      "disabled": true,
      "img": {
        "src": "/path/to/image.jpeg",
        "class": "image",
        "height": "24px"
      }
    }
  ]
}
```

#### Query parameters

For loading data from your endpoint the requests are built by default (`searchParam`, `pageParam`, `perPageParam`) like
this:

`your.url.with.protocol?q=some+query&_page=1&_limit=50`

## API

AsyncPick registers a global `window.AsyncPick` object that lets you access each initialized select. You can get each
instance by using its id:

```javascript
window.AsyncPick.elements // or just AsyncPick.elements
// => {'picker-single': AsyncPick}

window.AsyncPick.elements['picker-single']
// => AsyncPick {..}
```

Working on this object you can use any method and read any property that AsyncPick uses, but the only useful methods to
call externally are the following:

| method  | example                                | description                                                                                                              |
|---------|----------------------------------------|--------------------------------------------------------------------------------------------------------------------------|
| `destroy` | `AsyncPick.elements['picker'].destroy()` | Destroys the added dom elements and event handlers. Resets the select to it's state before async-pick was initialized. |
| `reload`  | `AsyncPick.elements['picker'].reload()`  | Same as destroy, but re-initializes the element automatically.                                                        |

Call a method directly:

```javascript
window.AsyncPick.elements['picker-single'].reload()
```

## Development

Install dependencies and start the server:

```bash
# install dependencies
yarn

# run mock api
yarn mock

# run development server
yarn dev
```

Then open [http://localhost:8000](http://localhost:8000).

Alternatively you can use the dev version directly in tbs by symlinking the current build:

```bash
cd async-pick

# remove existing lib in tbs
rm -rf path/to/tbs/node_modules/async-pick

# symlink built version
ln -sf $(pwd) path/to/tbs/node_modules/

# build after every change
yarn build
```

## Publishing a new version

1. Make your changes and create a new build with `yarn build`
2. Update the `version` in `package.json`
3. Commit
4. Push master
5. Create new tag on master `git tag -a v0.x.x`
6. Push tag `git push --tags`
7. Go to tbs and update async-pick version in `package.json`
8. Run `yarn` 
