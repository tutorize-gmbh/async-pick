import AsyncPickMarkup from './async-pick/async-pick-markup'
import de from './async-pick/i18n/de'
import en from './async-pick/i18n/en'
import es from './async-pick/i18n/es'
import fr from './async-pick/i18n/fr'

/**
 * AsyncPick
 * @param {Object}   options
 * @param {String}   options.id - id of the select to use
 * @param {String}   options.url - url to fetch data from
 * @param {String}  [options.language='en'] - set language. defaults to document language or 'en'
 * @param {String}  [options.searchParam='q'] - name of the query parameter for searching
 * @param {String}  [options.pageParam='_page'] - parameter to use for querying pages in the url
 * @param {String}  [options.perPageParam='_limit'] - parameter to use for setting page size in the url
 * @param {String}  [options.jsonKey=null] - name of the key where your nested json data lies under
 * @param {String}  [options.httpMethod='GET'] - http method to use for querying
 * @param {Number}  [options.maxPages=2] - number of pages to show simultaneously
 * @param {Number}  [options.perPage=50] - number of entries to show per page
 * @param {Number}  [options.paginateUpThreshold=20] - scroll percentage where to start loading the upper page
 * @param {Number}  [options.paginateDownThreshold=80] - scroll percentage where to start loading the lower page
 * @param {Number}  [options.searchTimeout=400] - timeout after the last keydown when to start searching
 * @param {String}  [options.searchPlaceholder='Search'] - placeholder for the search input
 * @param {Array}   [options.searchInputClasses=[]] - additional classes to add to the search input
 * @param {Boolean} [options.withSearch=true] - enable search functionality
 * @param {String}  [options.emptySelectButtonText=[]] - set the text of the select button before something is selected
 * @param {Array}   [options.buttonClasses=[]] - additional classes to add to the "open" button
 * @param {Array}   [options.buttonDisabledClasses=[]] - additional classes to add to the disabled button
 * @param {Array}   [options.buttonIconClasses=[]] - additional classes to add to "open" buttons icon
 * @param {Array}   [options.checkedIconClasses=[]] - additional classes to add to "checked" icon
 * @param {Array}   [options.listClasses=[]] - additional classes to add to each list built from loaded data
 * @param {String}  [options.selectedTextFormat=null] - set to 'count > X' where X is a number to show all texts if the
 *                                                      selected count is below X, otherwise show `selectedText`
 * @param {String}  [options.selectedTextVariable='%num%'] - set to some placeholder that is included in `selectedText`
 *                                                        that gets replaced by the actual count when displaying text
 * @param {String}  [options.selectedText=options.selectedTextVariable + 'selected'] - the actual text
 * @param {String}  [options.noResultsText='No results'] - text to display when api returns no results
 * @param {String}  [options.dividerText='Selected entries:'] - text to display inside of the divider in multiple
 * @param {Boolean} [options.resetOnClose=true] - choose to reset the content and reload when closing the popup
 * @param {String}  [options.container=null] - container to append the html to
 * @param {Boolean} [options.dropdownAlignRight=false] - set to true to align the dropdown on the right
 * @param {Boolean} [options.popupWidth='300px'] - set width for popup when container option is set
 * @param {Boolean} [options.multiple=false] - to enable multiple. alternatively set your select to multiple
 * @param {Boolean} [options.disabled=false] - to disabled the select. alternatively set your select to disabled
 * @param {Object}  [options.values] - hash with preselected values, overrides possible <option selected> elements.
 *                                    This is an alternative to adding actual <option selected> elements to the dom.
 *                                    Structure has to be {<value>: {text: <text>, disabled: <disabled>}}.
 *                                    Note that both <value> and <text> should be equal to the actual option values.
 * @param {Boolean} [options.debug=false] - enable to log debug messages
 * @param {Function} [options.customDebugHandler=null] - additional function to call when debug messages are logged.
 *                                                       Receives the instance of async-pick as first arg.
 * @constructor
 */
export default function AsyncPick (options) {
    this.id = options.id
    this.label = document.querySelector('label[for="' + this.id + '"]')
    if (this.isInitialized()) AsyncPick.elements[this.id].destroy()

    this.element = document.getElementById(this.id)
    this.url = options.url.replace(/&amp;/g, '&')
    this.language = options.language || document.documentElement.lang || 'en'
    this.i18n = AsyncPick.i18n ? AsyncPick.i18n[this.language] : {}

    this.searchParam = options.searchParam || 'q'
    this.pageParam = options.pageParam || '_page'
    this.perPageParam = options.perPageParam || '_limit'
    this.jsonKey = options.jsonKey || null
    this.httpMethod = options.httpMethod || 'GET'
    this.maxPages = options.maxPages || 2
    this.perPage = options.perPage || 50
    this.paginateUpThreshold = options.paginateUpThreshold || 20
    this.paginateDownThreshold = options.paginateDownThreshold || 80
    this.searchTimeout = options.searchTimeout || 400
    this.searchPlaceholder = options.searchPlaceholder || this.i18n.searchPlaceholder || 'Search'
    this.searchInputClasses = options.searchInputClasses || []
    this.withSearch = options.hasOwnProperty('withSearch') ? options.withSearch : true
    this.buttonClasses = options.buttonClasses || []
    this.emptySelectButtonText = options.emptySelectButtonText || this.i18n.emptySelectButtonText || 'Select'
    this.buttonDisabledClasses = options.buttonDisabledClasses || []
    this.buttonIconClasses = options.buttonIconClasses || ['fas', 'fa-fw', 'fa-caret-down']
    this.checkedIconClasses = options.checkedIconClasses || ['fas', 'fa-fw', 'fa-check']
    this.listClasses = options.listClasses || []
    this.selectedTextFormat = options.selectedTextFormat || null
    this.selectedTextVariable = options.selectedTextVariable || '%num%'
    this.selectedText = options.selectedText || this.i18n.selectedText || this.selectedTextVariable + ' selected'
    this.noResultsText = options.noResultsText || this.i18n.noResultsText || 'No results'
    this.dividerText = options.dividerText || this.i18n.dividerText || 'Selected entries:'
    this.resetOnClose = !!options.resetOnClose
    this.container = options.container || null
    this.dropdownAlignRight = !!options.dropdownAlignRight
    this.popupWidth = options.popupWidth || '300px'
    this.selectedValues = options.values || null
    this.debug = options.debug || false
    this.customDebugHandler = options.customDebugHandler || null

    this.multiple = !!this.element.multiple || !!options.multiple
    this.disabled = !!this.element.disabled || !!options.disabled
    this.resetPageProps()
    this.blankOption = {}
    this.previousSearchValue = ''
    this.open = false
    this.originalInnerHTML = this.element.innerHTML

    this.initialize()
    this.logDebugMessage('initialized with options:', this)
    return this
}
AsyncPick.i18n = { de, en, es, fr }
AsyncPick.elements = {}

AsyncPick.prototype.initialize = function () {
    this.markup = this.buildMarkup()
    if (!this.disabled) {
        this.addHandlers()
        this.addEvents()
    }
    this.setupSelectedValues()
    this.setupBlankOption()
    this.register()
    if (!this.disabled) this.loadFirstPage()
    this.markup.setButtonText(this.selectedValues)
}

AsyncPick.prototype.buildMarkup = function () {
    return new AsyncPickMarkup({
        element: this.element,
        multiple: this.multiple,
        disabled: this.disabled,
        searchPlaceholder: this.searchPlaceholder,
        searchInputClasses: this.searchInputClasses,
        withSearch: this.withSearch,
        buttonClasses: this.buttonClasses,
        buttonDisabledClasses: this.buttonDisabledClasses,
        emptySelectButtonText: this.emptySelectButtonText,
        buttonIconClasses: this.buttonIconClasses,
        checkedIconClasses: this.checkedIconClasses,
        listClasses: this.listClasses,
        selectedTextFormat: this.selectedTextFormat,
        selectedTextVariable: this.selectedTextVariable,
        selectedText: this.selectedText,
        noResultsText: this.noResultsText,
        dividerText: this.dividerText,
        container: this.container,
        dropdownAlignRight: this.dropdownAlignRight,
        popupWidth: this.popupWidth
    })
}

AsyncPick.prototype.addHandlers = function () {
    this.togglePopupHandler = this.togglePopup.bind(this)
    if (this.withSearch) this.searchHandler = this.search.bind(this)
    this.onEntriesListScrollHandler = this.onEntriesListScroll.bind(this)
    this.closePopupHandler = this.closePopup.bind(this)
    this.selectHandler = this.select.bind(this)
    this.stopPropagationHandler = function (e) {
        e.stopPropagation()
    }
    this.buttonKeyHandler = this.onButtonKeyDown.bind(this)
    this.markupKeyHandler = this.onMarkupKeyDown.bind(this)
    this.labelClickHandler = function (e) {
        e.preventDefault()
        this.markup.button.focus()
    }.bind(this)
    this.containerPositionHandler = this.markup.positionPopup.bind(this.markup)
    this.resetFormHandler = this.resetAndReload.bind(this)
}

AsyncPick.prototype.resetAndReload = function () {
    this.logDebugMessage('form reset! reloading async-pick')
    this.resetting = true
    this.element.innerHTML = this.originalInnerHTML
    this.reload()
}

AsyncPick.prototype.onButtonKeyDown = function (e) {
    if (e.keyCode === 13) { // enter
        e.preventDefault()
        this.togglePopup(e)
    }
}

AsyncPick.prototype.onMarkupKeyDown = function (e) {
    if (e.keyCode === 9 || e.keyCode === 27) { // tab || esc
        this.closePopupAndFocus()
    } else if (e.keyCode === 40) { // arrow down
        this.markup.focusNextEntry()
    } else if (e.keyCode === 38) { // arrow up
        this.markup.focusPreviousEntry()
    } else if (e.keyCode === 13) { // enter
        e.preventDefault()
        const li = this.markup.getSelected()
        if (li) this.select({ currentTarget: li })
    }
}

AsyncPick.prototype.addEvents = function () {
    this.markup.button.addEventListener('click', this.togglePopupHandler)
    if (this.withSearch) this.markup.searchInput.addEventListener('input', this.searchHandler)
    this.markup.resultsScrollWrapper.addEventListener('scroll', this.onEntriesListScrollHandler)
    this.markup.popup.addEventListener('click', this.stopPropagationHandler)
    this.markup.button.addEventListener('keydown', this.buttonKeyHandler)
    if (this.withSearch) {
        this.markup.popup.addEventListener('keydown', this.markupKeyHandler)
    } else {
        this.markup.button.addEventListener('keydown', this.markupKeyHandler)
    }
    document.addEventListener('click', this.closePopupHandler)
    window.addEventListener('resize', this.containerPositionHandler)
    window.addEventListener('scroll', this.containerPositionHandler)
    if (this.label) this.label.addEventListener('click', this.labelClickHandler)
    if (this.element.form) this.element.form.addEventListener('reset', this.resetFormHandler)
    let self = this
    onDomRemove(this.markup.element, function () {
        self.destroy()
    })
}

AsyncPick.prototype.removeEvents = function () {
    let self = this
    this.markup.button.removeEventListener('click', this.togglePopupHandler)
    if (this.withSearch) this.markup.searchInput.removeEventListener('input', this.searchHandler)
    this.markup.resultsScrollWrapper.removeEventListener('scroll', this.onEntriesListScrollHandler)
    Array.apply(null, this.markup.resultsWrapper.querySelectorAll('li')).forEach(function (li) {
        li.removeEventListener('click', self.selectHandler)
    })
    if (this.multiple) Array.apply(null, this.markup.selectedList.querySelectorAll('li')).forEach(function (li) {
        li.removeEventListener('click', self.selectHandler)
    })
    this.markup.popup.removeEventListener('click', this.stopPropagationHandler)
    this.markup.button.removeEventListener('keydown', this.buttonKeyHandler)
    this.markup.popup.removeEventListener('keydown', this.markupKeyHandler)
    document.removeEventListener('click', this.closePopupHandler)
    window.removeEventListener('resize', this.containerPositionHandler)
    window.removeEventListener('scroll', this.containerPositionHandler)
    if (this.label) this.label.removeEventListener('click', this.labelClickHandler)
    if (this.element.form) this.element.form.removeEventListener('reset', this.resetFormHandler)
}

AsyncPick.prototype.setupSelectedValues = function () {
    if (!this.selectedValues) {
        this.selectedValues = {}
        let self = this
        Array.apply(null, this.element.options).filter(function (option) {
            return option.selected
        }).forEach(function (option) {
            self.selectedValues[option.value] = {
                text: option.innerHTML,
                subtext: option.getAttribute('data-subtext'),
                disabled: option.disabled,
                img: JSON.parse(option.getAttribute('data-img'))
            }
        })
    }
}

AsyncPick.prototype.setupBlankOption = function () {
    let blank = Array.apply(null, this.element.options).filter(function (option) {
        return option.value === ''
    })[0]
    if (!!blank) this.blankOption[''] = blank.innerHTML
}

AsyncPick.prototype.loadFirstPage = function () {
    if (!this.loaded) this.paginateDown(this.afterFirstPageLoad)
}

AsyncPick.prototype.togglePopup = function () {
    this.open ? this.closePopupAndFocus() : this.openPopup()
}

AsyncPick.prototype.search = function () {
    if (!this.withSearch) return
    if (this.searchInputTimer) clearTimeout(this.searchInputTimer)

    let inputValue = this.markup.searchInput.value
    if (this.shouldSearch(inputValue)) {
        let self = this
        self.previousSearchValue = inputValue
        this.searchInputTimer = setTimeout(function () {
            self.resetPages()
            self.searchQuery = self.markup.searchInput.value
            self.paginateDown()
        }, self.searchTimeout)
    }
}

AsyncPick.prototype.openPopup = function () {
    this.markup.popup.classList.add('ap__popup--visible')
    this.open = true
    this.markup.open = true
    this.markup.positionPopup()
    if (this.withSearch) this.markup.searchInput.focus()
    if (this.resetOnClose) this.markup.scrollToTop()
}

AsyncPick.prototype.closePopupAndFocus = function (e) {
    this.closePopup(e)
    this.markup.button.focus()
}

AsyncPick.prototype.closePopup = function (e) {
    let clickedButton = e && (e.target === this.markup.button || e.target.parentElement === this.markup.button)
    if (this.open && !clickedButton) {
        this.markup.popup.classList.remove('ap__popup--visible')
        this.open = false
        this.markup.open = false
        if (!this.resetting && this.resetOnClose) this.resetPagesAndSearch()
    }
}

AsyncPick.prototype.onEntriesListScroll = function () {
    const percentage = getScrollPercent(this.markup.resultsScrollWrapper)
    if (this.shouldPaginateUp(percentage)) {
        this.paginateUp()
    } else if (this.shouldPaginateDown(percentage)) {
        this.paginateDown()
    }
}

AsyncPick.prototype.buildUrl = function (page) {
    if (this.url.indexOf('?') > -1) {
        return this.url + '&' + this.getDefaultUrlParams(page)
    } else {
        return this.url + '?' + this.getDefaultUrlParams(page)
    }
}

AsyncPick.prototype.paginateUp = function () {
    this.beforePaginateUp()
    const url = this.buildUrl(this.upperPage)
    request(this.httpMethod, url, this.afterPaginateUp, this)
}

AsyncPick.prototype.beforePaginateUp = function () {
    this.paginating = true
    this.paginatingUp = true
    if (this.upperPage > 1) this.upperPage--
    this.markup.topLoader.style.display = 'block'
}

AsyncPick.prototype.handleXhrResponse = function (xhr) {
    if (responseSuccess(xhr)) {
        try {
            const parsedJson = JSON.parse(xhr.responseText)
            return getObjectKey(parsedJson, this.jsonKey)
        } catch (e) {
            console.error('AsyncPick could not parse json! Please make sure that your server returns the correct response. Got the following:')
            console.error(xhr.responseText)
            return []
        }
    } else {
        console.error('AsyncPick did not get a valid server response. xhr:')
        console.error(xhr)
        return []
    }
}

AsyncPick.prototype.afterPaginateUp = function (xhr, self) {
    let json = self.handleXhrResponse(xhr)

    let scrollTop = 0
    if (self.pageCount < self.maxPages) {
        self.pageCount++
    } else {
        self.endReached = false
        scrollTop = self.markup.getScrollTopWithoutPage(self.lowerPage)
        self.markup.removePage(self.lowerPage)
        self.lowerPage--
    }

    if (self.upperPage === 1) self.topReached = true
    const pageUl = self.markup.prependEntries(json, self.upperPage, self.selectedValues, scrollTop)
    if (!self.searchQueryPresent() && self.upperPage === 1 && Object.keys(self.blankOption).length > 0) {
        self.markup.prependToPage(self.upperPage, self.blankOption, self.selectedValues)
    }
    self.addEventListenersForPage(pageUl)
    self.paginatingUp = false
    self.paginating = false
    self.markup.topLoader.style.display = 'none'
    self.logDebugMessage('Paginating up finished')
}

AsyncPick.prototype.paginateDown = function (callback) {
    this.beforePaginateDown()
    const url = this.buildUrl(this.lowerPage)
    request(this.httpMethod, url, callback || this.afterPaginateDown, this)
}

AsyncPick.prototype.beforePaginateDown = function () {
    this.paginating = true
    this.paginatingDown = true
    this.lowerPage++
    this.markup.bottomLoader.style.display = 'block'
}

AsyncPick.prototype.afterFirstPageLoad = function (xhr, self) {
    self.afterPaginateDown(xhr, self)
    self.loaded = true
    self.markup.enableButton()
    if (self.multiple) {
        self.markup.appendSelectedList()
        self.markup.renderSelectedList(self.selectedValues)
        Array.apply(null, self.markup.selectedList.querySelectorAll('li')).forEach(function (li) {
            li.addEventListener('click', self.selectHandler)
        })
    }
}

AsyncPick.prototype.afterPaginateDown = function (xhr, self) {
    let json = self.handleXhrResponse(xhr)

    let scrollTop = 0
    if (self.pageCount < self.maxPages) {
        self.pageCount++
    } else {
        self.topReached = false
        scrollTop = self.markup.getScrollTopWithoutPage(self.upperPage)
        self.markup.removePage(self.upperPage)
        self.upperPage++
    }
    if (json.length < self.perPage) self.endReached = true
    const pageUl = self.markup.appendEntries(json, self.lowerPage, self.selectedValues, scrollTop)
    if (!self.searchQueryPresent() && self.lowerPage === 1 && Object.keys(self.blankOption).length > 0) {
        self.markup.prependToPage(self.lowerPage, self.blankOption, self.selectedValues)
    }
    self.addEventListenersForPage(pageUl)
    self.paginatingDown = false
    self.paginating = false
    self.markup.bottomLoader.style.display = 'none'
    self.logDebugMessage('Paginating down finished')
}

AsyncPick.prototype.addEventListenersForPage = function (pageUl) {
    let self = this
    Array.apply(null, pageUl.querySelectorAll('li.ap__results-list__item[data-value]:not(.ap__results-list__item--disabled)')).forEach(function (li) {
        li.addEventListener('click', self.selectHandler)
    })
}

AsyncPick.prototype.resetPageProps = function () {
    this.pageCount = 0
    this.upperPage = 1
    this.lowerPage = 0
    this.topReached = true
    this.endReached = false
    this.paginatingUp = false
    this.paginatingDown = false
    this.paginating = false
    this.loaded = false
    this.searchInputTimer = null
}

AsyncPick.prototype.shouldPaginateUp = function (percentage) {
    return !this.paginating && !this.paginatingUp && !this.topReached && percentage <= this.paginateUpThreshold
}

AsyncPick.prototype.shouldPaginateDown = function (percentage) {
    return !this.paginating && !this.paginatingDown && !this.endReached && percentage >= this.paginateDownThreshold
}

AsyncPick.prototype.shouldSearch = function (newValue) {
    return this.withSearch && this.previousSearchValue !== newValue
}

AsyncPick.prototype.select = function (e) {
    const li = e.currentTarget
    const value = li.getAttribute('data-value')
    const optionData = {
        text: li.getAttribute('data-text'),
        subtext: li.getAttribute('data-subtext'),
        disabled: li.getAttribute('data-disabled'),
        img: JSON.parse(li.getAttribute('data-img'))
    }

    if (this.multiple) {
        this.toggleSelectedValue(value, optionData)
    } else {
        this.setSelectedValue(value, optionData)
    }
    this.markup.setButtonText(this.selectedValues)
    if (!this.multiple) this.closePopupAndFocus()
    this.triggerChange()
}

AsyncPick.prototype.triggerChange = function () {
    let event = new CustomEvent('Event', { detail: this.selectedValues })
    event.initEvent('change', true, true)
    this.element.dispatchEvent(event)
}

AsyncPick.prototype.toggleSelectedValue = function (value, optionData) {
    if (this.selectedValues[value]) {
        this.removeSelectedValue(value)
    } else {
        this.addSelectedValue(value, optionData)
    }
}

AsyncPick.prototype.addSelectedValue = function (value, optionData) {
    this.selectedValues[value] = optionData
    const newLi = this.markup.selectItem(value, optionData)
    if (newLi) newLi.addEventListener('click', this.selectHandler)
    if (this.withSearch) this.markup.searchInput.focus()
    this.logDebugMessage('Value added:', optionData)
}

AsyncPick.prototype.removeSelectedValue = function (value) {
    delete this.selectedValues[value]
    const removedLi = this.markup.deselectItem(value)
    if (removedLi) removedLi.removeEventListener('click', this.selectHandler)
    this.logDebugMessage('Value removed:', value)
}

AsyncPick.prototype.setSelectedValue = function (value, optionData) {
    this.removeSelectedValue(Object.keys(this.selectedValues)[0])
    this.element.innerHTML = '' // remove all remaining options that might have been added initially
    this.addSelectedValue(value, optionData)
    this.logDebugMessage('Value set:', optionData)
}

AsyncPick.prototype.logDebugMessage = function (msg, someObject) {
    if (this.debug) {
        if (msg) console.log('AsyncPick#' + this.id, msg)
        if (someObject) console.log(someObject)
        if (typeof this.customDebugHandler === 'function') this.customDebugHandler(this)
    }
}

AsyncPick.prototype.getDefaultUrlParams = function (page) {
    let params = {}
    params[this.perPageParam] = this.perPage
    params[this.pageParam] = page
    if (this.searchQueryPresent()) params[this.searchParam] = this.searchQuery
    return objectToUrlParam(params)
}

AsyncPick.prototype.searchQueryPresent = function () {
    if (!this.withSearch) return false
    const query = this.markup.searchInput.value
    return typeof query !== 'undefined' && query.trim().length > 0
}

AsyncPick.prototype.register = function () {
    AsyncPick.elements[this.id] = this
}

AsyncPick.prototype.isInitialized = function () {
    if (typeof window.AsyncPick === 'undefined') {
        return false
    } else {
        return !!AsyncPick.elements[this.id]
    }
}

AsyncPick.prototype.resetPages = function () {
    this.markup.clean()
    this.resetPageProps()
}

AsyncPick.prototype.resetPagesAndSearch = function () {
    if (this.withSearch) this.markup.searchInput.value = ''
    this.resetPages()
    this.paginateDown()
}

AsyncPick.prototype.destroy = function () {
    if (!this.disabled) this.removeEvents()
    if (this.markup) this.markup.destroy()
    this.selectedValues = {}
    delete this.markup
    delete AsyncPick.elements[this.id]
    this.element.classList.remove('visually-hidden')
}

AsyncPick.prototype.reload = function () {
    this.destroy()
    this.resetPageProps()
    this.multiple = !!this.element.multiple
    this.disabled = !!this.element.disabled
    this.selectedValues = null
    this.initialize()
}

/**
 * Helpers
 */

function getScrollPercent (element) {
    return (element.scrollTop / (element.scrollHeight - element.clientHeight) * 100)
}

function objectToUrlParam (object) {
    let params = []
    Object.keys(object).forEach(function (key) {
        params.push(key + '=' + encodeURIComponent(object[key]))
    })
    return params.join('&')
}

function request (method, url, handler, context) {
    let xhr = new XMLHttpRequest()
    xhr.addEventListener('load', function () {
        handler(this, context)
    })
    xhr.open(method, url)
    xhr.setRequestHeader('Accept', 'application/json')
    xhr.send()
}

function getObjectKey (object, key) {
    if (!key) return object
    return object[key]
}

function responseSuccess (xhr) {
    return xhr.readyState === 4 && xhr.status.toString().match(/2\d\d/)
}

function onDomRemove (element, onDetachCallback) {
    const observer = new MutationObserver(function () {
        function isDetached (el) {
            if (el.parentNode === document) {
                return false
            } else if (el.parentNode === null) {
                return true
            } else {
                return isDetached(el.parentNode)
            }
        }

        if (isDetached(element)) {
            observer.disconnect()
            onDetachCallback()
        }
    })

    observer.observe(document, {
        childList: true,
        subtree: true
    })
}
