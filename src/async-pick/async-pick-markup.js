export default function AsyncPickMarkup (options) {
    this.element = options.element
    this.multiple = options.multiple || false
    this.disabled = options.disabled || false
    this.searchPlaceholder = options.searchPlaceholder
    this.searchInputClasses = options.searchInputClasses
    this.withSearch = options.withSearch
    this.buttonClasses = options.buttonClasses
    this.buttonDisabledClasses = options.buttonDisabledClasses
    this.emptySelectButtonText = options.emptySelectButtonText
    this.buttonIconClasses = options.buttonIconClasses
    this.checkedIconClasses = options.checkedIconClasses
    this.listClasses = options.listClasses
    this.selectedTextFormat = options.selectedTextFormat
    this.selectedTextVariable = options.selectedTextVariable
    this.selectedText = options.selectedText
    this.noResultsText = options.noResultsText
    this.dividerText = options.dividerText
    this.container = options.container
    this.dropdownAlignRight = options.dropdownAlignRight
    this.popupWidth = options.popupWidth
    this.open = false

    this.wrapper = this.buildWrapper()
    this.button = this.buildButton()
    if (!this.disabled) this.popup = this.buildPopup()

    this.hideOriginalSelect()
    this.assemble()
}

AsyncPickMarkup.prototype.hideOriginalSelect = function () {
    this.element.classList.add('visually-hidden')
    this.element.setAttribute('tabindex', '-1')
}

AsyncPickMarkup.prototype.showOriginalSelect = function () {
    this.element.style.display = 'initial'
}

AsyncPickMarkup.prototype.assemble = function () {
    this.wrapper.appendChild(this.button)
    if (!this.disabled) {
        if (!!this.container) {
            const container = document.querySelector(this.container)
            if (container) {
                container.appendChild(this.popup)
                this.popup.style.position = 'absolute'
                this.popup.style.top = '0'
            }
        } else {
            this.wrapper.appendChild(this.popup)
        }
    }
    this.element.parentNode.insertBefore(this.wrapper, this.element)
}

AsyncPickMarkup.prototype.positionPopup = function () {
    if (!this.open) return
    if (!!this.container) {
        const pos = this.wrapper.getBoundingClientRect()
        const offset = getOffsetFromBoundingBox(pos)
        const top = offset.top + this.wrapper.offsetHeight

        if (this.shouldDropUp()) {
            this.popup.style.top = (top - this.popup.offsetHeight - this.button.offsetHeight - parseInt(window.getComputedStyle(this.popup).marginTop)).toString() + 'px'
        } else {
            this.popup.style.top = top.toString() + 'px'
        }
        if (this.dropdownAlignRight) {
            const right = window.innerWidth - pos.left - this.wrapper.offsetWidth
            this.popup.style.right = right + 'px'
        } else {
            this.popup.style.left = offset.left.toString() + 'px'
        }
        this.popup.style.minWidth = this.wrapper.offsetWidth + 'px'
        this.popup.style.width = this.popupWidth
        this.popup.style.position = 'absolute'
    } else {
        if (this.shouldDropUp()) {
            this.popup.style.top = 'auto'
            this.popup.style.bottom = (this.button.offsetHeight + parseInt(window.getComputedStyle(this.popup).marginTop)).toString() + 'px'
        } else {
            this.popup.style.top = '100%'
            this.popup.style.bottom = 'auto'
        }
    }
}

AsyncPickMarkup.prototype.shouldDropUp = function () {
    const pos = this.button.getBoundingClientRect()
    const offset = getOffsetFromBoundingBox(pos)
    const selectOffsetTop = offset.top - document.documentElement.scrollTop
    const selectOffsetBot = window.innerHeight - selectOffsetTop - this.button.offsetHeight
    return selectOffsetTop > selectOffsetBot && selectOffsetBot < this.popup.offsetHeight
}

AsyncPickMarkup.prototype.buildWrapper = function () {
    const wrapper = document.createElement('div')
    wrapper.classList.add('async-pick')
    if (this.multiple) wrapper.classList.add('async-pick--multiple')
    return wrapper
}

AsyncPickMarkup.prototype.buildButton = function () {
    const button = document.createElement('button')
    button.setAttribute('type', 'button')
    this.buttonClasses.forEach(function (buttonClass) {
        button.classList.add(buttonClass)
    })
    button.disabled = true
    if (this.disabled) {
        this.buttonDisabledClasses.forEach(function (buttonDisabledClass) {
            button.classList.add(buttonDisabledClass)
        })
    }

    this.buttonText = document.createElement('span')
    this.buttonText.classList.add('ap__button-text')

    if (this.buttonIconClasses && this.buttonIconClasses.length > 0) {
        let buttonIcon = document.createElement('i')
        this.buttonIconClasses.forEach(function (buttonIconClass) {
            buttonIcon.classList.add(buttonIconClass)
        })
        button.appendChild(this.buttonText)
        button.appendChild(buttonIcon)
    } else {
        button.appendChild(this.buttonText)
    }

    return button
}

AsyncPickMarkup.prototype.enableButton = function () {
    this.button.disabled = false
}

AsyncPickMarkup.prototype.buildPopup = function () {
    const popup = document.createElement('div')
    popup.classList.add('ap__popup')
    if (this.dropdownAlignRight) popup.classList.add('ap__popup--right')

    if (this.withSearch) popup.appendChild(this.buildSearchInput())
    popup.appendChild(this.buildResultsScrollWrapper())

    return popup
}

AsyncPickMarkup.prototype.buildSearchInput = function () {
    const wrapper = document.createElement('div')
    wrapper.classList.add('ap__search-input__wrapper')
    this.searchInput = document.createElement('input')
    this.searchInput.type = 'search'
    this.searchInput.setAttribute('placeholder', this.searchPlaceholder)
    this.searchInput.classList.add('ap__search-input')

    let self = this
    this.searchInputClasses.forEach(function (searchInputClass) {
        self.searchInput.classList.add(searchInputClass)
    })

    wrapper.appendChild(this.searchInput)
    return wrapper
}

AsyncPickMarkup.prototype.buildResultsScrollWrapper = function () {
    this.resultsScrollWrapper = document.createElement('div')
    this.resultsScrollWrapper.classList.add('ap__results-scroll-wrapper')
    this.resultsWrapper = document.createElement('div')
    this.resultsWrapper.classList.add('ap__results')
    this.resultsWrapper.setAttribute('aria-role', 'list')
    this.resultsScrollWrapper.appendChild(this.buildTopLoader())
    this.resultsScrollWrapper.appendChild(this.resultsWrapper)
    this.resultsScrollWrapper.appendChild(this.buildBottomLoader())
    return this.resultsScrollWrapper
}

AsyncPickMarkup.prototype.buildTopLoader = function () {
    this.topLoader = document.createElement('div')
    this.topLoader.classList.add('ap-top-loader')
    this.topLoader.innerHTML = '...'
    this.topLoader.style.display = 'none'
    return this.topLoader
}

AsyncPickMarkup.prototype.buildBottomLoader = function () {
    this.bottomLoader = document.createElement('div')
    this.bottomLoader.classList.add('ap-bottom-loader')
    this.bottomLoader.innerHTML = '...'
    this.bottomLoader.style.display = 'none'
    return this.bottomLoader
}

AsyncPickMarkup.prototype.appendSelectedList = function () {
    this.popup.appendChild(this.buildDivider())
    this.popup.appendChild(this.buildSelectedList())
}

AsyncPickMarkup.prototype.buildDivider = function () {
    const divider = document.createElement('div')
    divider.classList.add('ap__divider')
    divider.innerHTML = this.dividerText
    return divider
}

AsyncPickMarkup.prototype.buildSelectedList = function () {
    const selected = document.createElement('div')
    selected.classList.add('ap__selected-scroll-wrapper')
    this.selectedList = buildUl(this.listClasses)
    selected.appendChild(this.selectedList)
    return selected
}

AsyncPickMarkup.prototype.renderSelectedList = function (selectedValues) {
    let self = this

    this.selectedList.innerHTML = ''
    Object.keys(selectedValues).forEach(function (value) {
        const li = buildLi({ value, ...selectedValues[value] })
        self.selectedList.appendChild(li)
    })
}

AsyncPickMarkup.prototype.appendEntries = function (newEntries, lowerPage, selectedValues, scrollTopOffset) {
    const pageUl = buildUl(this.listClasses, lowerPage)
    this.renderNewEntries(newEntries, pageUl, selectedValues)

    const scrollableOffset = scrollTopOffset > 0 ? scrollTopOffset : this.resultsScrollWrapper.scrollTop

    this.resultsWrapper.appendChild(pageUl)
    this.resultsScrollWrapper.scrollTop = scrollableOffset
    return pageUl
}

AsyncPickMarkup.prototype.prependEntries = function (newEntries, upperPage, selectedValues, scrollTopOffset) {
    const pageUl = buildUl(this.listClasses, upperPage)
    this.renderNewEntries(newEntries, pageUl, selectedValues)

    const scrollableOffset = scrollTopOffset > 0 ? scrollTopOffset : this.resultsScrollWrapper.scrollTop

    this.resultsWrapper.insertBefore(pageUl, this.resultsWrapper.firstChild)
    this.resultsScrollWrapper.scrollTop = scrollableOffset + pageUl.offsetHeight
    return pageUl
}

AsyncPickMarkup.prototype.renderNewEntries = function (newEntries, ul, selectedValues) {
    let self = this
    if (newEntries.length > 0) {
        newEntries.forEach(function (element) {
            const selected = !!selectedValues[element.value]
            const li = buildLi({
                ...element,
                selected,
                addCheck: self.multiple,
                checkedIconClasses: self.checkedIconClasses
            })
            ul.appendChild(li)
        })
    } else {
        const li = buildLi({ text: this.noResultsText })
        li.classList.add('ap__results-list__item--muted')
        ul.appendChild(li)
    }
}

AsyncPickMarkup.prototype.renderSelectedOption = function (value, text) {
    const option = document.createElement('option')
    option.value = value
    option.innerHTML = text
    option.selected = true
    option.setAttribute('data-selected', '')
    return option
}

AsyncPickMarkup.prototype.removeSelectedOption = function (value) {
    const self = this
    Array.apply(null, this.element.options).filter(function (option) {
        return option.value === value || option.value.toString() === value.toString()
    }).forEach(function (option) {
        self.element.removeChild(option)
    })
}

AsyncPickMarkup.prototype.selectItem = function (value, optionData) {
    const option = this.renderSelectedOption(value, optionData.text)
    this.element.appendChild(option)
    this.addSelectedClassByValue(value)
    if (this.multiple) return this.addToSelectedList(value, optionData)
}

AsyncPickMarkup.prototype.deselectItem = function (value) {
    this.removeSelectedOption(value)
    this.removeSelectedClassByValue(value)
    if (this.multiple) return this.removeFromSelectedList(value)
}

AsyncPickMarkup.prototype.addToSelectedList = function (value, optionData) {
    const li = buildLi({ value, text: optionData.text, subtext: optionData.subtext, img: optionData.img })
    this.selectedList.appendChild(li)
    return li
}

AsyncPickMarkup.prototype.removeFromSelectedList = function (value) {
    const li = this.selectedList.querySelector('li.ap__results-list__item[data-value="' + value + '"]')
    if (li) this.selectedList.removeChild(li)
    return li
}

AsyncPickMarkup.prototype.prependToPage = function (page, blankOptions, selectedValues) {
    const ul = this.resultsWrapper.querySelector('ul[data-page="' + page + '"]')
    if (!ul) return

    const self = this
    Object.keys(blankOptions).forEach(function (value) {
        const selected = !!selectedValues[value]
        const li = buildLi({
            value,
            text: blankOptions[value],
            selected,
            addCheck: self.multiple,
            checkedIconClasses: self.checkedIconClasses
        })
        ul.insertBefore(li, ul.firstChild)
    })
}

AsyncPickMarkup.prototype.removePage = function (page) {
    const ul = this.resultsWrapper.querySelector('ul[data-page="' + page.toString() + '"]')
    if (ul) this.resultsWrapper.removeChild(ul)
}

AsyncPickMarkup.prototype.addSelectedClassByValue = function (value) {
    const li = this.resultsWrapper.querySelector('li[data-value="' + value + '"]')
    if (li) setLiSelected(li, true, this.multiple, this.checkedIconClasses)
}

AsyncPickMarkup.prototype.removeSelectedClassByValue = function (value) {
    const li = this.resultsWrapper.querySelector('li[data-value="' + value + '"]')
    if (li) setLiSelected(li, false, this.multiple, this.checkedIconClasses)
}

AsyncPickMarkup.prototype.destroy = function () {
    this.showOriginalSelect()
    if (!!this.container) {
        const container = document.querySelector(this.container)
        if (container) container.removeChild(this.popup)
    }
    if (this.element && this.element.parentNode) this.element.parentNode.removeChild(this.wrapper)
}

AsyncPickMarkup.prototype.clean = function () {
    let self = this
    Array.apply(null, this.resultsWrapper.querySelectorAll('ul')).forEach(function (ul) {
        self.resultsWrapper.removeChild(ul)
    })
}

AsyncPickMarkup.prototype.scrollToTop = function () {
    this.resultsScrollWrapper.scrollTop = 0
}

AsyncPickMarkup.prototype.getScrollTopWithoutPage = function (page) {
    const ul = this.resultsWrapper.querySelector('ul[data-page="' + page.toString() + '"]')
    if (ul) {
        return this.resultsScrollWrapper.scrollTop - ul.scrollHeight
    } else {
        return this.resultsScrollWrapper.scrollTop
    }
}

AsyncPickMarkup.prototype.getSelected = function () {
    return this.resultsWrapper.querySelectorAll('li.ap__results-list__item--selected')[0]
}

AsyncPickMarkup.prototype.focusPreviousEntry = function () {
    const selected = this.getSelected()
    if (selected) {
        const prev = selected.previousSibling || (selected.parentNode && selected.parentNode.previousSibling && selected.parentNode.previousSibling.lastChild)
        if (prev) {
            selected.classList.remove('ap__results-list__item--selected')
            prev.classList.add('ap__results-list__item--selected')
            this.scrollEntryIntoView(prev)
        }
    } else {
        const first = this.resultsWrapper.querySelectorAll('li.ap__results-list__item[data-value]')[0]
        if (!first) return
        first.classList.add('ap__results-list__item--selected')
        this.scrollEntryIntoView(first)
    }
}

AsyncPickMarkup.prototype.focusNextEntry = function () {
    const selected = this.getSelected()
    if (selected) {
        const next = selected.nextSibling || (selected.parentNode && selected.parentNode.nextSibling && selected.parentNode.nextSibling.firstChild)
        if (next) {
            selected.classList.remove('ap__results-list__item--selected')
            next.classList.add('ap__results-list__item--selected')
            this.scrollEntryIntoView(next)
        }
    } else {
        const first = this.resultsWrapper.querySelectorAll('li.ap__results-list__item[data-value]')[0]
        if (!first) return
        first.classList.add('ap__results-list__item--selected')
        this.scrollEntryIntoView(first)
    }
}

AsyncPickMarkup.prototype.scrollEntryIntoView = function (entry) {
    const entryOffsetTop = entry.offsetTop - this.resultsScrollWrapper.offsetTop
    const shouldScrollDown = this.resultsScrollWrapper.offsetHeight + this.resultsScrollWrapper.scrollTop < entryOffsetTop + entry.offsetHeight
    const shouldScrollUp = entryOffsetTop < this.resultsScrollWrapper.scrollTop

    if (shouldScrollDown) {
        this.resultsScrollWrapper.scrollTop = entryOffsetTop - this.resultsScrollWrapper.offsetHeight + entry.offsetHeight
    } else if (shouldScrollUp) {
        this.resultsScrollWrapper.scrollTop = entryOffsetTop
    }
}

AsyncPickMarkup.prototype.setButtonText = function (selectedValues) {
    if (selectedValues && Object.keys(selectedValues).length > 0) {
        this.buttonText.innerHTML = this.renderButtonText(selectedValues)
    } else {
        this.buttonText.innerHTML = this.emptySelectButtonText
    }
}

AsyncPickMarkup.prototype.renderButtonText = function (selectedValues) {
    if (this.multiple && this.selectedTextFormat) {
        const match = this.selectedTextFormat.match(/count\s?>\s?([0-9]*)/)
        const count = match && match[1] && parseInt(match[1])

        if (count && count < Object.keys(selectedValues).length) {
            return this.selectedText.replace(this.selectedTextVariable, Object.keys(selectedValues).length)
        } else {
            return joinSelectedTexts(selectedValues)
        }
    } else {
        return joinSelectedTexts(selectedValues)
    }
}

function joinSelectedTexts (selectedValues) {
    let selected = []
    Object.keys(selectedValues).forEach(function (value) {
        selected.push(selectedValues[value].text)
    })
    return selected.join(', ')
}

function buildUl (additionalClasses, page) {
    const pageUl = document.createElement('ul')
    pageUl.classList.add('ap__results-list')
    pageUl.setAttribute('aria-role', 'listbox')
    additionalClasses.forEach(function (listClass) {
        pageUl.classList.add(listClass)
    })
    if (page) pageUl.setAttribute('data-page', page.toString())
    return pageUl
}

function buildLi (options) {
    const text = options.text
    const value = options.value
    const disabled = options.disabled
    const subtext = options.subtext
    const imgAttributes = options.img
    const li = document.createElement('li')

    const textSpan = document.createElement('span')
    textSpan.classList.add('ap__results-list__item__text')
    textSpan.innerHTML = text

    if (typeof value !== 'undefined' && value !== null) li.setAttribute('data-value', value)
    if (typeof text !== 'undefined' && text !== null) {
        li.setAttribute('aria-label', text)
        li.setAttribute('title', textSpan.innerText)
        li.setAttribute('data-text', text)
    }
    if (typeof disabled !== 'undefined' && disabled !== null) {
        li.setAttribute('data-disabled', disabled)
        if (disabled) li.classList.add('ap__results-list__item--disabled')
    }
    if (typeof subtext !== 'undefined' && subtext !== null) {
        li.setAttribute('data-subtext', subtext)
        const subtextDom = document.createElement('small')
        subtextDom.innerHTML = subtext
        subtextDom.classList.add('ap__results-list__item__subtext')
        textSpan.appendChild(subtextDom)
    }

    li.setAttribute('aria-role', 'listitem')
    li.classList.add('ap__results-list__item')

    if (typeof imgAttributes !== 'undefined' && imgAttributes !== null) {
        const imageTag = document.createElement('img')
        Object.keys(imgAttributes).forEach(function (attr) {
            imageTag.setAttribute(attr, imgAttributes[attr])
        })

        imageTag.classList.add('ap__results-list__item__image')
        li.setAttribute('data-img', JSON.stringify(imgAttributes))
        li.appendChild(imageTag)
    }

    li.appendChild(textSpan)
    if (options.selected) {
        setLiSelected(li, true, options.addCheck, options.checkedIconClasses)
    }
    return li
}

function setLiSelected (li, selected, addCheck, checkedIconClasses) {
    if (selected) {
        if (addCheck) {
            const check = document.createElement('i')
            checkedIconClasses.forEach(function (checkedIconClass) {
                check.classList.add(checkedIconClass)
            })
            check.classList.add('ap__results-list__item__check-mark')
            li.appendChild(check)
        } else {
            li.classList.add('ap__results-list__item--selected')
        }
    } else {
        if (addCheck) {
            const check = li.querySelector('.ap__results-list__item__check-mark')
            if (check) li.removeChild(check)
        } else {
            li.classList.remove('ap__results-list__item--selected')
        }
    }
}

function getOffsetFromBoundingBox (box) {
    const docElem = document.documentElement

    return {
        top: box.top + (window.pageYOffset || docElem.scrollTop) - (docElem.clientTop || 0),
        left: box.left + (window.pageXOffset || docElem.scrollLeft) - (docElem.clientLeft || 0)
    }
}
