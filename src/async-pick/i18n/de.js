export default {
    searchPlaceholder: 'Suchen',
    emptySelectButtonText: 'Alle anzeigen',
    noResultsText: 'Keine Ergebnisse gefunden',
    selectedText: '%num% Einträge ausgewählt',
    dividerText: 'Ausgewählte Einträge:'
}
