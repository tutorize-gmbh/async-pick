export default {
    searchPlaceholder: 'Search',
    emptySelectButtonText: 'Show all',
    noResultsText: 'No results found',
    selectedText: '%num% selected',
    dividerText: 'Selected entries:'
}
