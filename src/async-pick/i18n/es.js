export default {
    searchPlaceholder: 'Buscar',
    emptySelectButtonText: 'Mostrar todos',
    noResultsText: 'No se encontraron resultados',
    selectedText: '%num% seleccionados',
    dividerText: 'Entradas seleccionadas:'
}
