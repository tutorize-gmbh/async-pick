export default {
    searchPlaceholder: 'Rechercher',
    emptySelectButtonText: 'afficher Tout',
    noResultsText: 'Aucun résultat trouvé',
    selectedText: '%num% sélectionnés',
    dividerText: 'Éléments sélectionnés:'
}
